import java.time.LocalDate;
import java.util.Scanner;

public class Main{
    public static String getMonthName(int month){
        switch(month){
            case 1:
                return "Januari";
            case 2:
                return "Febuari";
            case 3:
                return "Maret";
            case 4:
                return "April";
            case 5:
                return "Mei";
            case 6:
                return "Juni";
            case 7:
                return "Juli";
            case 8:
                return "Agustus";
            case 9:
                return "September";
            case 10:
                return "Oktober";
            case 11:
                return "November";
            case 12:
                return "Desember";
            default:
                return "bulan";
        }
    }
    public static void main(String[] args) {
        
        LocalDate now = LocalDate.now();
        Scanner userInput = new Scanner(System.in);
       

        System.out.println("Tanggal : " + now);
        System.out.println("----------- Mendaftar NetFlix -----------");
        System.out.println("Ingatkan pembayaran sebelum 1-7 Hari");
        System.out.println("Silahakan pilih : ");
        int remindDay = userInput.nextInt();
        

        for (LocalDate date = now; date.isBefore(now.plusYears(1).plusDays(1)); date = date.plusDays(1))
        {
            System.out.println("Tanggal Hari Ini : " + date);
            if(date.getDayOfMonth() == now.minusDays(remindDay).getDayOfMonth() ){
                System.out.println(date.getDayOfMonth() + getMonthName(date.getMonthValue()) + date.getYear() +" -> Email Dikirim");
            }
        }

        

    }

}