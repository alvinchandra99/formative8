import java.io.FileOutputStream;
import java.io.IOException;


class Main{
    public static void main(String[] args) {
        String defaultText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."+
        "Maecenas dictum nisl sed mi tempus tristique. Suspendisse eu convallis nunc."+ 
        "In hac habitasse platea dictumst. Mauris placerat quis nulla ac rhoncus."+ 
        "Mauris blandit nisl sit amet facilisis rhoncus. Suspendisse et posuere neque."+ 
        "Quisque sapien lectus, posuere id nulla eget, convallis mollis tellus."+ 
        "Duis convallis sapien ut nunc facilisis fermentum." +
        "Nulla placerat massa quis mattis rutrum." +
        "Interdum et malesuada fames ac ante ipsum primis in faucibus." +
        "Aenean scelerisque luctus eros ac volutpat..";

        String replaceText = defaultText.replaceAll("L","7").replaceAll("i", "1").replaceAll("p", "8").replaceAll("o", "9").replaceAll("s","5" );
        try{
            FileOutputStream day821 = new FileOutputStream("day821.txt");        
            FileOutputStream day822 = new FileOutputStream("day822.txt");
    
            String withoutNumber = replaceText.replaceAll("[\\s\\.\\d]+", "");
            String withoutAlphabet = replaceText.replaceAll("\\D+", "");
    
            day821.write(withoutAlphabet.getBytes());
            day822.write(withoutNumber.getBytes());
    
    
            day821.close();
            day822.close();
        
            
        }
        catch(IOException e){

        }
        
    
    
    }
}